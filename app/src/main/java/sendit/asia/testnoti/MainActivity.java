package sendit.asia.testnoti;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationDisplayedResult;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OSNotificationReceivedResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.math.BigInteger;

public class MainActivity extends AppCompatActivity {

    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();
        OneSignal.startInit(this)
//                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
//                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationOpenedHandler(new MyNotificationOpenedHandler())
                .setNotificationReceivedHandler( new MyNotificationReceivedHandler())
                .init();

//        OneSignal.sendTag("", "");
    }

    class MyNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
        // This fires when a notification is opened by tapping on it.
        @Override
        public void notificationOpened(OSNotificationOpenResult result) {
            OSNotificationAction.ActionType actionType = result.action.type;
            JSONObject data = result.notification.payload.additionalData;
            String activityToBeOpened;
//
//            //While sending a Push notification from OneSignal dashboard
//            // you can send an addtional data named "activityToBeOpened" and retrieve the value of it and do necessary operation
//            //If key is "activityToBeOpened" and value is "AnotherActivity", then when a user clicks
//            //on the notification, AnotherActivity will be opened.
//            //Else, if we have not set any additional data MainActivity is opened.
//            if (data != null) {
//                activityToBeOpened = data.optString("activityToBeOpened", null);
//                if (activityToBeOpened != null &amp;&amp; activityToBeOpened.equals("AnotherActivity")) {
//                    Log.i("OneSignalExample", "customkey set with value: " + activityToBeOpened);
//                    Intent intent = new Intent(MyApplication.getContext(), AnotherActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    MyApplication.getContext().startActivity(intent);
//                } else if (activityToBeOpened != null &amp;&amp; activityToBeOpened.equals("MainActivity")) {
//                    Log.i("OneSignalExample", "customkey set with value: " + activityToBeOpened);
//                    Intent intent = new Intent(MyApplication.getContext(), MainActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    MyApplication.getContext().startActivity(intent);
//                } else {
//                    Intent intent = new Intent(MyApplication.getContext(), MainActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    MyApplication.getContext().startActivity(intent);
//                }
//
//            }
//
//            //If we send notification with action buttons we need to specidy the button id's and retrieve it to
//            //do the necessary operation.
//            if (actionType == OSNotificationAction.ActionType.ActionTaken) {
//                Log.i("OneSignalExample", "Button pressed with id: " + result.action.actionID);
//                if (result.action.actionID.equals("ActionOne")) {
//                    Toast.makeText(MyApplication.getContext(), "ActionOne Button was pressed", Toast.LENGTH_LONG).show();
//                } else if (result.action.actionID.equals("ActionTwo")) {
//                    Toast.makeText(MyApplication.getContext(), "ActionTwo Button was pressed", Toast.LENGTH_LONG).show();
//                }
//            }
        }
    }


    class MyNotificationReceivedHandler  implements OneSignal.NotificationReceivedHandler {
        @Override
        public void notificationReceived(OSNotification notification) {
            JSONObject data = notification.payload.additionalData;
            String customKey;

            if (data != null) {
                //While sending a Push notification from OneSignal dashboard
                // you can send an addtional data named "customkey" and retrieve the value of it and do necessary operation
                customKey = data.optString("customkey", null);
                if (customKey != null)
                    Log.i("OneSignalExample", "customkey set with value: " + customKey);
            }
        }

    }



}
